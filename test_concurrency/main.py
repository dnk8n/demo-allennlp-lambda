import csv
import json
import logging
import os
from pathlib import Path
import sys
import traceback
from typing import Dict

from allennlp.predictors.predictor import Predictor
from allennlp.common.tqdm import Tqdm
import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)

for var in ['NLTK_DATA', 'HOME', 'TMPDIR']:
    val = os.getenv(var)
    if val:
        Path(val).mkdir(parents=True, exist_ok=True)
        logger.info(json.dumps({"exists": {val: True}}))

correfor = Predictor.from_path(os.getenv('CORREFOR', 'https://storage.googleapis.com/allennlp-public-models/coref-spanbert-large-2021.03.10.tar.gz'))


def handle_exception():
    exception_type, exception_value, exception_traceback = sys.exc_info()
    traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
    err_msg = json.dumps(
        {
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        }
    )
    logger.error(err_msg)


def load_csv(filepath):
    with open(filepath, newline='') as csvfile:
        return list(csv.DictReader(csvfile))


def handler_dispatch(event: Dict, context: Dict):
    try:
        csv_data = load_csv('./data/corpus.csv')
        client_lambda = boto3.client('lambda')
        for article_payload in csv_data:
            response = client_lambda.invoke(
                FunctionName='test_concurrency_process',
                InvocationType='Event',
                Payload=json.dumps(article_payload).encode('utf-8')
            )
            response["Payload"] = response["Payload"].read().decode("utf-8")
            message = json.dumps({"response": response})
            logger.info(message)
        return {"status": "success", "message": message}
    except Exception:
        handle_exception()


def handler_process(event: Dict, context: Dict):
    try:
        txt = event['text']
        corefoutput = correfor.predict(txt)
        logger.info(json.dumps({"corefoutput": corefoutput}))
        return {"status": "success", "message": corefoutput}
    except Exception:
        handle_exception()