## Quick and dirty steps, how ./data/corpus.csv was produced
- [OPTIONAL]: If you use podman instead of Docker, in each new shell:
  - `export BUILDAH_FORMAT=docker; alias docker=podman`
- `docker build -t demo-allennlp-lambda:latest .`
- `docker run --entrypoint python --rm -it --name demo-allennlp-lambda demo-allennlp-lambda:latest`
- >>> `from generate_corpus.main import write_csv; write_csv()`
- In a new shell:
  - `docker cp demo-allennlp-lambda:./corpus.csv ./data/`

I couldn't work out how to make the CSV output reroducible. If you manage, please let me know!
