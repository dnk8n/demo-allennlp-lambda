import csv
import re

from transformers import pipeline


def write_csv():
    generator = pipeline('text-generation', model='gpt2')

    fields = ['text']
    rows = [
        [
            ''.join(
                re.split('(\.!?)', text["generated_text"])[:-1]
            )
        ]
        for text in generator("In today's news,", max_length=100, num_return_sequences=500)
    ]
    rows_10000 = rows*20  # Scales to 10,000 csv rows to test out how AWS Lambda or EC2 instance handles scale
    
    with open('./corpus.csv', 'w') as csvfile:  # Hardcoded as this is a demo, please see README on intended docker/podman run command
        csvwriter = csv.writer(csvfile) 
        csvwriter.writerow(fields) 
        csvwriter.writerows(rows)
