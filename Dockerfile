FROM public.ecr.aws/lambda/python:3.9

RUN \
    yum install -y clang make && \
    python -m pip install --upgrade pip && \
    pip install wheel

RUN pip install torch==1.10.1+cpu torchvision==0.11.2+cpu torchaudio==0.10.1+cpu -f https://download.pytorch.org/whl/cpu/torch_stable.html

COPY requirements.txt .
RUN pip install -r requirements.txt

RUN python -m spacy download en_core_web_sm

COPY .   .
