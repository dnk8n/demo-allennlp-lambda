> Note, this experiment is now **outdated**. Modern AWS Lambda does not have any of the limitations discussed. And so it is therefore feasible to perform a fan-out/fan-in pipeline architecture. I implemented this architecture in a job successfully. Unfortunately I can't share the code.

# Introduction

> Note, if you want to run similar processing code in a local environment instead of Lambda, see the [alternate README](/../non-lambda/README.md)

This demo started off as a feasibility prototype for an idea to scale a processing pipeline involving AllenNLP and an NLP procedure called Coreference Resolution. The current process was taking a week on a desktop computer.

Before you get too far into this, you might want to first know that it was a failed experiment. My conclusion was that it is more feasible to batch the input data and automate the requesting of ephemeral cloud processing infrastructure, like multiple spot instances with output data streamed to S3. AllenNLP supports GPUs so probably the best processing speedup would be found there.

Nevertheless I go on to describe the experiment. By the way, see here how I [generated the demo corpus CSV](/generate_corpus/README.md).

The idea was to create a dispatch lambda function that iterated over the rows of a single CSV containing thousands (500 for the demo) of large articles (much larger than in the demonstration), asyncronously calling one processing lambda function invocation per row, to try and parallelize the processing using the power of serverless. This all sounds pretty reasonable, especially with the new abilities to deploy AWS Lambda functions as containers, 15min timeouts, and 10GB RAM limits.

# What contributed to the experiment's failure

The main reason is due to the amount of data each Lambda invocation reads/writes to disk. AllenNLP, upon initialization unarchives to /tmp or the location defined in environment variable TMPDIR. First problem is that Lambda has a 500MB limit at /tmp, which is insufficient for our needs.

So we work out that we can use EFS for shared storage. Great. Problem is that AllenNLP unarchives data on disk simultaneously for each of the 500 invocations, which overwhelms EFS as shown in the following image.

![EFS overwhelmed](/readme/efs-overwhelmed.png)

This causes a disk IO bottleneck, causing all the processing to grind to a halt and none of the processing Lambda functions to complete. The takeaway here is that the architecture cannot rely on a shared disk being accessed simultaneously by more than one invocation.

So in a different use-case, if you can get away with < 500MB disk usage, then Lambda is still an option (and because no NAT gateway / EFS setup is required it is very cost effective even at small scale).

If more than 500MB is required and the dispatch function can perform all necessary disk IO so that very little reading is required by processing invocations, then Lambda is also still an option (although cost-wise unfeasible unless processing at a very large scale), assuming that EFS can easily handle the concurrent disk processing.

But because neither of the above two points are true, i.e. because AllenNLP performs large quantities of read and write operations, AWS Lambda becomes infeasible for this use-case.

It is also suspected that with large documents, 10GB RAM will itself also be insufficient, even if there was no disk IO issue.

# Costs you should be aware of

Another issue that comes in with the use of EFS is that you are then forced to create a VPC with public and private subnets, with fairly complicated configuration of internet gateways, NAT gateways, etc. If you set this demo up, and then never use it again you are looking at a cost of $50-60 per month. A third of this cost is due to temporary files not cleaned up due to timed out Lambda invocations and two thirds is the cost of a NAT gateway. The cost of Lambda itself is negligible.

# Conclusion

It is simpler to setup automation that can batch the input data and get multiple spot instances to process on fast instance storage (rather than slow network storage like EFS). Once processed output files can be saved in S3 and the entire infrastructure can be destroyed, as it can be treated as ephemeral.

Long story short, perhaps Lambda can be useful in this way for other libraries but not for this use-case.

# Demo

NB: Proceed with caution, remember to clean up NAT gateway and EFS file system

## Steps to configure AWS Lambda

- Pre-requisites:
  - You have an IAM user with API access and appropriate access to AWS Lambda, ECR, EC2, VPC, EFS, etc (not recommended but potentially helpful policies: AWSLambda_FullAccess, AmazonEC2ContainerRegistryFullAccess, AmazonEC2FullAccess, AmazonVPCFullAccess, AmazonElasticFileSystemFullAccess)
  - Recommended: Install direnv (although you can also just remember to source .envrc each time)

- Create ECR repo:
  - Go to https://console.aws.amazon.com/ecr
  - Set region (e.g. N. Virginia - us-east-1)
  - Create Repository
  - Visibility settings: Private
  - Repository name: demo-allennlp-lambda
  - Create Repository

- Configure .envrc:
  - `cp .envrc.tpl .envrc`
  - Fill in environment variables (set AWS_DEFAULT_REGION if you create ECR repo in another region; Find ECR_URI from ECR page of web console, but subtract '/<repository name>')
  - If you have direnv configured, `direnv allow .` (once-off)
  - Else, each time you build image remember to `. .envrc`

- `./image-build.sh`

- Create Lambda functions:
  - Go to https://console.aws.amazon.com/lambda
  - First function:
    - Create function
    - Container image
    - Function name: test_concurrency_dispatch
    - Container image URI: Browse images > select repository > demo-allennlp-lambda > latest
    - Container image overrides: CMD: 'handlers.handler_dispatch'
    - Create function
  - Second function:
    - Create function
    - Container image
    - Function name: test_concurrency_process
    - Container image URI: Browse images > select repository > demo-allennlp-lambda > latest
    - Container image overrides: CMD: 'handlers.handler_process'
    - Create function

- Creat VPC by following https://blog.theodo.com/2020/01/internet-access-to-lambda-in-vpc/
  - For subnet availability zone, specify 'a', e.g. 'us-east-1a' (This simplifies EFS connection for demo. Not suitable for production)
  - Be careful, it is easy to accidentally choose IGW in place of NAT
  - Follow instructions to test that your Lambda Function has internet connection:
    - https://aws.amazon.com/premiumsupport/knowledge-center/ssm-troubleshoot-lambda-internet-access/
    - You can choose 1.1.1.1 as destination, for FunctionName copy ARN of test_concurrency_dispatch, leave other fields as defaults
    - Be sure to scrutinize the outputs, don't rely on success statuses alone

- Create EFS
  - Go to https://console.aws.amazon.com/efs
  - Create file system
  - VPC: my-wonderful-vpc
  - Availability and durability: One Zone (Not suitable for production)
  - Availability Zone: as in previous step

- Create Access point to EFS
  - Open file system as created in previous step
  - Access points tab > Create access point
  - Root directory path: /test_concurrency
  - POSIX user > User ID: 1001
  - POSIX user > Group ID: 1001
  - Root directory creation permissions > Owner user ID: 1001
  - Root directory creation permissions > Owner group ID: 1001
  - POSIX permissions to apply to the root directory path: 0755
  - Create Access Point

- Modify each Lambda execution roles with permissions to call CreateNetworkInterface on EC2:
  - Go to https://console.aws.amazon.com/iam
  - Roles
  - Start typing 'dispatch', 'process' as appropriate to filter role
  - Select filtered role
  - Attach Policies
  - Policy name: AWSLambdaVPCAccessExecutionRole
  - Attach Policies

- Repeat previous step for 'dispatch' role
  - Policy name: AWSLambdaRole

- Modify each Lambda function:
  - Configuration > General configuration > Edit
  - Memory: 10240 MB
  - Timeout: 15 min 0 sec
  ___
  - Configuration > Environment variables > Edit
  - NLTK_DATA=/mnt/efs/home/nltk_data
  - HOME=/mnt/efs/home
  - TMPDIR=/mnt/efs/tmp
  ___
  - Configuration > VPC > Edit
  - VPC: Select 'my-wonderful-vpc'
  - Subnets Select 'my-wnderful-vpc-private-subnet'
  - Security groups: 'default VPC security group'
  ___
  - Configuration > File systems > Add file system
  - EFS file system: As created in previous step
  - Access point: As created in previous step
  - Local mount path: /mnt/efs

## Test dispatch Lambda Function (test_concurrency_dispatch)

- This will invoke one instance of process Lambda Function (test_concurrency_process) per line in the CSV baked into the lambda image (500 articles)
- To initiate, open test_concurrency_dispatch
- Test (tab) > Test (Event can be anything)

## Follow logs while Test dispatch Lambda Function is running

- In a new tab, go to https://console.aws.amazon.com/cloudwatch
- Log groups > /aws/lambda/test_concurrency_dispatch
- Choose the appropriate logstream, based on time range where test invocation was initiated
- Click on 'Resume' at the bottom
