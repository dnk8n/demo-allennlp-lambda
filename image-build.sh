#!/usr/bin/env bash
set -ueo pipefail

shopt -s expand_aliases

if ! command -v docker &> /dev/null
then
    echo "[WARNING]: Docker not installed, reverting to Podman..."
    if ! command -v podman &> /dev/null
    then
        echo "[ERROR]: One of Docker or Podman needs to be installed... exiting"
        exit
    else
        export BUILDAH_FORMAT=docker
        alias docker=podman
    fi
fi

aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin ${ECR_URI}
docker build -t ${ECR_URI}/${ECR_REPO}:latest .
docker push ${ECR_URI}/${ECR_REPO}:latest
